# _Projeto exemplo de arquitetura hexagonal_

## _Esse projeto tem por finalidade servir de guia para implantaçãode projetos com arquitetura hexagonal._

### _Passos para implementação do projeto:_

- A primeira camada a ser chamada será a do entrypoint. Essa camada receberá a requisição e chamará o usecase. O
  entrypoint não deverá conhecer os objetos das outras camadas, sendo assim se faz necessário a conversão dos tipos
  nesse pacote. Exemplo: Se recebermos um objeto PessoaModelRequest como parâmetro da requisição, deveremos converte-lo
  para **_PessoaDomainRequest_** e só assim passar para a próxima camada(usecase).
- A segunda camada chamada de usecase fará o intermédio entre o entrypoint e a camada de persistencia dos dados (
  dataprovider)
  através da interface do gateway. A camada seguinte conhece o objeto **_PessoaDomainRequest_**.
- A terceira e última camada, dataprovider (camada de persistência), também fará a conversão dos objetos chegados, de
  PessoaDomainRequest em **_PessoaEntity_**. O resultado, que será uma entidade do banco de dados, deverá ser convertida para
  domain que seguirá o seguinte caminho: dataprovider (convertEntityToDomain) -> usecase -> Domain ->
  entrypoint (convertDomainToModel) e a resposta chegará ao fim do ciclo que no caso do exemplo deverá ser
  **_PessoaModelResponse_**. 
  