package br.com.banzo;

import br.com.banzo.dataprovider.repository.entity.Aluno;
import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
public class AlunoDTO {

    private String nome;
    private List<String> professor;

    public static AlunoDTO convert(Aluno aluno) {
        return AlunoDTO
                .builder()
                .professor(aluno.getAvaliacoes().stream().map(alunos -> alunos.getProfessor().getNome()).collect(Collectors.toList()))
                .nome(aluno.getNome())
                .build();
    }
}
