package br.com.banzo;

import br.com.banzo.dataprovider.repository.ProfessorRepository;
import br.com.banzo.dataprovider.repository.entity.Aluno;
import br.com.banzo.dataprovider.repository.AlunoRepeostory;
import br.com.banzo.dataprovider.repository.entity.Endereco;
import br.com.banzo.dataprovider.repository.entity.Professor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class Test {

    @Autowired
    private AlunoRepeostory alunoRepeostory;

    @Autowired
    private ProfessorRepository professorRepository;

    @GetMapping("/aluno")
    public ResponseEntity<Aluno> salvarAluno() {
        Endereco end = Endereco
                .builder()
                .cep("3333")
                .cidade("hellcife")
                .complemento("apt803")
                .estado("Pe")
                .logradouro("Rua Viriato Correia")
                .numero(22)
                .build();
        Aluno alun = alunoRepeostory.save(Aluno
                .builder()
                .nome("Ant")
                .email("ant@gmail.com")
                .endereco(end)
                .nascimento(LocalDateTime.of(1980, 02, 18, 19, 30, 12))
                .telefone("000000")
                .build());
        Professor prof = Professor
                .builder()
                .nome("JOSE")
                .cref("03540/G-PE")
                .build();

        professorRepository.save(prof);
        return ResponseEntity.ok(alun);
    }

    @GetMapping("/aluno2")
    public ResponseEntity<List<AlunoDTO>> salvarAluno2() {
        List<AlunoDTO> collect = alunoRepeostory.findAll().stream().map(aluno -> AlunoDTO.convert(aluno)).collect(Collectors.toList());
        return ResponseEntity.ok(collect);
    }
}
