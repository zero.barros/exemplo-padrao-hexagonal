package br.com.banzo.dataprovider.implementation;

import br.com.banzo.dataprovider.mapper.PessoaDataProviderMapper;
import br.com.banzo.dataprovider.repository.PessoaRepository;
import br.com.banzo.dataprovider.repository.entity.PessoaEntity;
import br.com.banzo.usecase.domain.request.PessoaDomainRequest;
import br.com.banzo.usecase.domain.response.PessoaDomainResponse;
import br.com.banzo.usecase.gateway.PessoaGateway;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static br.com.banzo.dataprovider.mapper.PessoaDataProviderMapper.toPessoaEntity;

@Component
public class PessoaDataProvider implements PessoaGateway {

    private final PessoaRepository pessoaRepository;

    public PessoaDataProvider(PessoaRepository pessoaRepository) {
        this.pessoaRepository = pessoaRepository;
    }

    @Override
    public Optional<PessoaDomainResponse> buscaPessoaPorid(Integer id) {

        Optional<PessoaEntity> pessoaEntity = pessoaRepository.findById(id);

        return pessoaEntity.map(PessoaDataProviderMapper::toPessoaDomain);
    }

    @Override
    public List<PessoaDomainResponse> listarPessoas() {
        List<PessoaEntity> listaPessoa = pessoaRepository.findAll();

        return listaPessoa.stream().map(PessoaDataProviderMapper::toPessoaDomain).collect(Collectors.toList());
    }

    @Override
    public void salvarPessoa(PessoaDomainRequest pessoaDomainRequest) {
        pessoaRepository.save(toPessoaEntity(pessoaDomainRequest));
    }

}
