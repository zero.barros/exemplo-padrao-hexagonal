package br.com.banzo.dataprovider.mapper;

import br.com.banzo.dataprovider.repository.entity.PessoaEntity;
import br.com.banzo.dataprovider.repository.entity.TipoPessoa;
import br.com.banzo.usecase.domain.request.PessoaDomainRequest;
import br.com.banzo.usecase.domain.response.PessoaDomainResponse;

public class PessoaDataProviderMapper {


    public static PessoaDomainResponse toPessoaDomain(PessoaEntity pessoaEntity) {
        return PessoaDomainResponse
                .builder()
                .id(pessoaEntity.getId())
                .nome(pessoaEntity.getNome())
                .cpf(pessoaEntity.getCpf())
                .cnpj(pessoaEntity.getCnpj())
                .email(pessoaEntity.getEmail())
                .build();
    }
    public static PessoaEntity toPessoaEntity(PessoaDomainRequest pessoaDomainRequest) {
        TipoPessoa tipoPessoa = TipoPessoa.builder().id(pessoaDomainRequest.getTipo()).build();
        return PessoaEntity
                .builder()
                .nome(pessoaDomainRequest.getNome())
                .tipo(tipoPessoa)
                .cnpj(pessoaDomainRequest.getCnpj())
                .cpf(pessoaDomainRequest.getCpf())
                .email(pessoaDomainRequest.getEmail())
                .build();
    }

}
