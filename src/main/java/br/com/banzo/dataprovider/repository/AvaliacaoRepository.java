package br.com.banzo.dataprovider.repository;

import br.com.banzo.dataprovider.repository.entity.Avaliacao;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AvaliacaoRepository extends JpaRepository<Avaliacao, Integer> {
}
