package br.com.banzo.dataprovider.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "avaliacao")
public class Avaliacao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name="aluno_id", nullable=false)
    private Aluno aluno;

    @ManyToOne
    @JoinColumn(name="professor_id", nullable=false)
    private Professor professor;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "perimetria_id", referencedColumnName = "id")
    private Perimetria perimetria;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "dobra_id", referencedColumnName = "id")
    private Dobra dobra;

    private Double peso;

    private Double altura;

    private LocalDateTime dataAvaliacao;

    private LocalDateTime dataProximaAvaliacao;

}
