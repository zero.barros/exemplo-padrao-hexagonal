package br.com.banzo.dataprovider.repository.entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Dobra {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Double subclavicular;
    private Double triciptal;
    private Double biciptal;
    private Double axilarMedia;
    private Double abdominal;
    private Double supraIliaca;
    private Double coxaMedial;

    @OneToOne(mappedBy = "dobra")
    private Avaliacao avaliacao;

}
