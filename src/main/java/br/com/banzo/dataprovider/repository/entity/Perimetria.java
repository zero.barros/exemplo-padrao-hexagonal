package br.com.banzo.dataprovider.repository.entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Perimetria {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Double peitoral;

    private Double cintura;

    private Double quadril;

    private Double bicepsDireito;

    private Double bicepsEsquerdo;

    private Double antebracoDireito;

    private Double antebracoEsquerdo;

    private Double coxaDireita;

    private Double coxaEsquerda;

    private Double panturrilhaDireita;

    private Double panturrilhaEsquerda;

    @OneToOne(mappedBy = "perimetria")
    private Avaliacao avaliacao;
}
