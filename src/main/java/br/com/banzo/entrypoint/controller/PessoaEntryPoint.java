package br.com.banzo.entrypoint.controller;

import br.com.banzo.entrypoint.mapper.PessoaEntryPointMapper;
import br.com.banzo.entrypoint.model.request.PessoaModelRequest;
import br.com.banzo.entrypoint.model.response.PessoaModelResponse;
import br.com.banzo.usecase.domain.request.PessoaDomainRequest;
import br.com.banzo.usecase.service.PessoaUseCase;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class PessoaEntryPoint {

    private final PessoaUseCase pessoaUseCase;

    public PessoaEntryPoint(PessoaUseCase pessoaUseCase) {
        this.pessoaUseCase = pessoaUseCase;
    }

    @GetMapping("/pessoa/{id}")
    public ResponseEntity<PessoaModelResponse> buscaPessoaPorid(@PathVariable("id") Integer id) {
        PessoaModelResponse pessoaModelResponse = this.pessoaUseCase.buscaPessoaPorid(id)
                .map(PessoaEntryPointMapper::toPessoaModelResponse)
                .orElseThrow(() -> new HttpClientErrorException(HttpStatus.NOT_FOUND));
        return ResponseEntity.ok(pessoaModelResponse);
    }

    @GetMapping("/pessoa")
    public ResponseEntity<List<PessoaModelResponse>> listarPessoas() {
        List<PessoaModelResponse> listaPessoas = this.pessoaUseCase.listarPessoas()
                .stream()
                .map(PessoaEntryPointMapper::toPessoaModelResponse)
                .collect(Collectors.toList());
        return ResponseEntity.ok(listaPessoas);
    }

    @PostMapping("/pessoa")
    public ResponseEntity salvarPessoa(@RequestBody PessoaModelRequest pessoaModelRequest) {

        PessoaDomainRequest pessoaDomainRequest = PessoaEntryPointMapper.toPessoaDomainRequest(pessoaModelRequest);
        this.pessoaUseCase.salvarPessoa(pessoaDomainRequest);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
