package br.com.banzo.entrypoint.controller.exception;


import br.com.banzo.entrypoint.model.response.MenssagemErroModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class CostumizerResponseExceptionHandle extends ResponseEntityExceptionHandler {

    @ExceptionHandler(HttpClientErrorException.class)
    public final ResponseEntity<Object> handleNaoEncontrado(HttpClientErrorException exception, WebRequest webRequest){
        MenssagemErroModel messageErrorModel = MenssagemErroModel.builder()
                .codigo(String.valueOf(HttpStatus.NOT_FOUND.value()))
                .mensagem("Recurso inexistente")
                .build();
        return new ResponseEntity<>(messageErrorModel, HttpStatus.NOT_FOUND);
    }
}
