package br.com.banzo.entrypoint.mapper;

import br.com.banzo.entrypoint.model.request.PessoaModelRequest;
import br.com.banzo.entrypoint.model.response.PessoaModelResponse;
import br.com.banzo.usecase.domain.request.PessoaDomainRequest;
import br.com.banzo.usecase.domain.response.PessoaDomainResponse;

public class PessoaEntryPointMapper {

    public static PessoaModelResponse toPessoaModelResponse(PessoaDomainResponse pessoaDomainResponse) {
        return PessoaModelResponse
                .builder()
                .id(pessoaDomainResponse.getId())
                .nome(pessoaDomainResponse.getNome())
                .cpf(pessoaDomainResponse.getCpf())
                .cnpj(pessoaDomainResponse.getCnpj())
                .email(pessoaDomainResponse.getEmail())
                .build();
    }

    public static PessoaDomainRequest toPessoaDomainRequest(PessoaModelRequest pessoaModelRequest) {
        return PessoaDomainRequest
                .builder()
                .nome(pessoaModelRequest.getNome())
                .tipo(pessoaModelRequest.getTipo())
                .cnpj(pessoaModelRequest.getCnpj())
                .cpf(pessoaModelRequest.getCpf())
                .email(pessoaModelRequest.getEmail())
                .build();
    }
}
