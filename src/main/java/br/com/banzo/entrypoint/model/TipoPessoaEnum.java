package br.com.banzo.entrypoint.model;

public enum TipoPessoaEnum {
    FISICA(1, "Pessoa Física"), JURIDICA(2, "Pessoa Jurídica");

    private int valor;

    private String nome;

    TipoPessoaEnum(int valor, String nome) {
        this.valor = valor;
        this.nome = nome;
    }
    public int getValor() {
        return valor;
    }
    public String getNome() {
        return nome;
    }
}
