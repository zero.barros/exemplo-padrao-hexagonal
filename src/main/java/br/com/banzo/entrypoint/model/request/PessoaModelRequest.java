package br.com.banzo.entrypoint.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PessoaModelRequest {

    private String nome;

    private int tipo;

    private String cpf;

    private String cnpj;

    private String email;
}
