package br.com.banzo.entrypoint.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
@AllArgsConstructor
public class CampoMensagemErroModel {

    private String campo;
    private String mensagem;
    private String valor;
}
