package br.com.banzo.entrypoint.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@Builder
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MenssagemErroModel {

    private String codigo;
    private String mensagem;
    private List<CampoMensagemErroModel> compos;
}
