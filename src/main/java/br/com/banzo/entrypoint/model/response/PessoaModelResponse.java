package br.com.banzo.entrypoint.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PessoaModelResponse {

    private Integer id;

    private String nome;

    private String cpf;

    private String cnpj;

    private String email;
}
