package br.com.banzo.usecase.domain.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PessoaDomainRequest {

    private String nome;

    private int tipo;

    private String cpf;

    private String cnpj;

    private String email;
}
