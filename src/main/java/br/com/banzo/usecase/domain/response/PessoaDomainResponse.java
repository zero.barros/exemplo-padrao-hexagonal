package br.com.banzo.usecase.domain.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PessoaDomainResponse {

    private Integer id;

    private String nome;

    private String cpf;

    private String cnpj;

    private String email;
}
