package br.com.banzo.usecase.gateway;

import br.com.banzo.usecase.domain.request.PessoaDomainRequest;
import br.com.banzo.usecase.domain.response.PessoaDomainResponse;

import java.util.List;
import java.util.Optional;

public interface PessoaGateway {

    Optional<PessoaDomainResponse> buscaPessoaPorid(Integer id);
    List<PessoaDomainResponse> listarPessoas();
    void salvarPessoa(PessoaDomainRequest pessoaDomainRequest);
}
