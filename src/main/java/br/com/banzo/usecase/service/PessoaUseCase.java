package br.com.banzo.usecase.service;

import br.com.banzo.usecase.domain.request.PessoaDomainRequest;
import br.com.banzo.usecase.domain.response.PessoaDomainResponse;
import br.com.banzo.usecase.gateway.PessoaGateway;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class PessoaUseCase {

    private final PessoaGateway pessoaGateway;

    public PessoaUseCase(PessoaGateway pessoaGateway) {
        this.pessoaGateway = pessoaGateway;
    }

    public Optional<PessoaDomainResponse> buscaPessoaPorid(Integer id){
        return pessoaGateway.buscaPessoaPorid(id);
    }

    public List<PessoaDomainResponse> listarPessoas(){
        return pessoaGateway.listarPessoas();
    }

    public void salvarPessoa(PessoaDomainRequest pessoaDomainRequest){
        pessoaGateway.salvarPessoa(pessoaDomainRequest);
    }
}
