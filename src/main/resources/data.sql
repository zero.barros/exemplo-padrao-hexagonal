insert into pessoa (nome, tipo, cpf, cnpj, email) values
('jose', 1,'304.937.950-22', null, 'ze@gmail.com'),
('carlão', 2, null,'20.607.679/0001-99', 'carlao@gmail.com');

insert into tipo_pessoa (id, nome) values
(1, 'JURÍDICA'),
(2, 'FÍSICA');

--insert into aluno (nome, nascimento, telefone, email) values ('jose', '1980-12-13', '96759966', 'zero.barros@gmail.com');