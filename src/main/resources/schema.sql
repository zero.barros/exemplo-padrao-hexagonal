drop table if exists pessoa;
create table pessoa (
    id integer not null auto_increment,
    nome varchar(30),
    tipo int not null,
    cpf varchar(14),
    cnpj varchar(18),
    email varchar(30),
    primary key (id)
);


drop table if exists tipo_pessoa;
create table tipo_pessoa (
    id integer not null,
    nome varchar(8),
    primary key (id)
);


--drop table if exists aluno;
--create table aluno (
--    id integer not null auto_increment,
--    nome varchar(30),
--    nascimento date,
--    telefone varchar(15),
--    email varchar(30),
--    primary key (id)
--);
--
--drop table if exists professor;
--create table professor (
--    id integer not null auto_increment,
--    nome varchar(8),
--    cref varchar(8),
--    primary key (id)
--);
