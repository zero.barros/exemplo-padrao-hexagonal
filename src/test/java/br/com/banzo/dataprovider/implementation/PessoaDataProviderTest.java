package br.com.banzo.dataprovider.implementation;

import br.com.banzo.dataprovider.repository.PessoaRepository;
import br.com.banzo.dataprovider.repository.entity.PessoaEntity;
import br.com.banzo.usecase.domain.response.PessoaDomainResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.context.jdbc.Sql.*;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
@TestPropertySource(locations = "classpath:application.properties")
@Sql(
        executionPhase = ExecutionPhase.BEFORE_TEST_METHOD,
        scripts = {"classpath:schema.sql", "classpath:data.sql"}
)
public class PessoaDataProviderTest {

    @InjectMocks
    private PessoaDataProvider pessoaDataProvider;

    @Mock
    private PessoaRepository pessoaRepository;

    @Test
    public void buscaPessoaPorid(){

        BDDMockito.given(pessoaRepository.findById(Mockito.anyInt())).willReturn(mockPessoaEntity());
        Optional<PessoaDomainResponse> pessoaDomainResponseOpt = pessoaDataProvider.buscaPessoaPorid(1);

        PessoaDomainResponse pessoaDomainResponse = pessoaDomainResponseOpt.get();
        assertNotNull(pessoaDomainResponse);
        assertEquals(1, pessoaDomainResponse.getId());

    }

    private Optional<PessoaEntity> mockPessoaEntity() {
        PessoaEntity pessoaEntity = new PessoaEntity();
        pessoaEntity.setId(1);
        pessoaEntity.setNome("jose");
        pessoaEntity.setCpf("99988877733");
        pessoaEntity.setEmail("ze@gmail.com");
        return Optional.of(pessoaEntity);
    }

}